
/*Función para iniciar el proceso del ejercicio*/

const btnCalcualr = function(){
    /*Creación de la función para calcular el promedio del arreglo*/

    function prome (arreglo){
        let promedio = 0;

        for(let i=0; i < 20; i++){
            promedio = arreglo[i] + promedio;
        }

        return promedio / 20;
    }

    /*Creación de la función para conocer los pares*/
    function par(arreglo){
        let pares = 0;

        for(let i=0; i < 20; i++){
            if((arreglo[i] % 2) == 0){
                pares++;
            }
        }

        return pares;
    }

    /*Creación de la función para orenar de Mayor a menor el arreglo*/
    const nuevOrden = (arreglo) => {
        let ordenado = arreglo.slice();
        return  ordenado.sort((a,b) => b - a);
    }

    /*Función para la impresion de los arreglos*/
    const imprimir = function(arreglo, elemen){
        let contenedor = "";
        arreglo.forEach(item => {
            contenedor += `<p>${item}</p>\n`;
        });
        elemen.innerHTML = contenedor;

    }

    /*Conseguir elementos en el arreglo*/
    const arreglo = [];
    for(let i = 0; i < 20; i++){
        arreglo.push(Math.floor(Math.random() * (150 - 1) + 1))
    }

    /*Declaración de variables y llamadas a las funciones*/
    const promedio = prome(arreglo);
    const pares = par(arreglo);
    const ordenado = nuevOrden(arreglo);

    /*Imprimir los datos*/
    document.getElementById('pro').value = promedio.toString();
	document.getElementById('par').value = pares.toString();
    
    imprimir(arreglo, document.getElementById("arregloOri"));
	imprimir(ordenado, document.getElementById("arregloOrd"));
};

document.getElementById("btnCalcular").addEventListener('click', btnCalcualr);